1. Cloned the angular 2 seed: https://github.com/mgechev/angular2-seed

Going forward we will try to cover these topics from here:
http://yakovfain.com/2015/05/28/started-working-on-the-core-angular-2-book/

1. Introducing Angular 2
  The landscape of client-side development
The history of Angular
Why choosing Angular 2
Architecture of Angular 2 applications
Introducing a sample Online Auction

2. TypeScript as a language for Angular applications
TypeScript as a superset of ECMAScript 6
Tooling

3. Getting started with Angular
Creating your first single-page application
Model-View-Component
Templates
Data binding
Setting up the development environment for Angular 2 applications
Hands-on: Getting started with Online Auction

4. Registering and creating objects
The Dependency Injection pattern
Angular Modules
Hands-on: Modularizing Online Auction

5. Navigation with Component Router
Changing views
Hash-based navigation
Deep-linking with HTML5 History API
Passing Parameters to Components
Authentication and the role-based navigation
Lazy-Loading Modules
Hands-on: Adding navigation to Online Auction

6. Bringing Together Data and Views with Data Binding
Connecting UI to the data
Benefits of data binding
Unidirectional vs. two-way data binding
Change detection
Data Binding in Templates
Hands-on: Binding data to views in Online Auction

7. User Interaction via Forms
Data entry
Form controls
Data-Driven Forms
From UI-driven to data-driven forms
Form validation
Hands-on: Offering products for sale

8. Communicating with Servers
Asynchronous HTTP requests
Working with RESTful services
Using WebSockets
Hands-on: Pushing bid notifications to the clients

9. Testing Angular applications
Developing without a compiler
Test runners
Testing frameworks
Working with mock objects
Troubleshooting Angular applications
In-browser debugging
Hands-on: Testing Online Auction

10. Deploying Angular Applications
Configuring Web servers for single page applications
Supporting HTML5 History API
Proxying HTTP requests
Cross-origin resource sharing
Hands-on: Deploying Online Auction

Appendix A. JavaScript Implementation of ECMAScript 6
The Scope of a Variable
Arrow functions
Rest parameters
Classes
Collections
Modules
Destructuring
Promises
Generators
Code conversion from ES6 to ES5 with Babel

Appendix B. Web Components
Shadow DOM
Templates
Custom Elements
HTML Imports
Using Web Components in Angular
Using Polymer elements

